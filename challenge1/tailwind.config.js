module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  theme: {
    extend: {
      colors: {
        "very-dark-blue": "hsl(217, 19%, 35%)",
        "dark-blue": "hsl(214, 17%, 51%)",
        "blue": "hsl(212, 23%, 69%)",
        "light-blue": "hsl(210, 46%, 95%)"
      },
      fontFamily: {
        "Manrope": ["Manrope", "sans-serif"]
      }
    },
  },
  variants: {},
  plugins: [],
}
