module.exports = {
  purge: [],
  theme: {

    extend: {
      fontFamily: {
        'liber': ['Libre Franklin', 'sans-serif'],
      },
      colors: {
        "primary-blue": "hsl(223, 87%, 63%)",
        "secondary-blue": "hsl(223, 100%, 88%)",
        "light-red": "hsl(354, 100%, 66%)",
        "m-gray": "hsl(0, 0%, 59%)",
        "dark-blue": "hsl(209, 33%, 12%)"
      }
    },
  },
  variants: {},
  plugins: [],
}
