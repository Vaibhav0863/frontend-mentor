module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        "open-sans": ["Open Sans", "sans-serif"],
        "poppins": ["Poppins", "sans-serif"]
      },
      colors: {
        "primary-pink": "hsl(322, 100%, 66%)",
        "pale-cyan": "hsl(193, 100%, 96%)",
        "dark-gray": "hsl(192, 100%, 9%)",
        "grayish-blue": "hsl(208, 11%, 55%)"
      }
    },
  },
  variants: {},
  plugins: [],
}
