module.exports = {
  purge: [],
  theme: {
    extend: {
      fontFamily: {
        inter: ['Inter', "sans - serif"],
      },
      colors: {
        "dark-blue": "hsl(240, 38%, 20%)",
        "grayish-blue": "hsl(240, 18%, 77%)",
      }
    },
  },
  variants: {},
  plugins: [],
}
