module.exports = {
  purge: [],
  theme: {

    extend: {
      fontFamily: {
        "josefin": ["Josefin Sans", "sans-serif"],
      },
      colors: {
        'reed': "hsl(0, 36%, 70%)",
        'soft-red': "hsl(0, 93%, 68%)",
        'dark-red': "hsl(0, 6%, 24%)"
      }
    },
  },
  variants: {},
  plugins: [],
}
